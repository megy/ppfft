import numpy as np
import pytest

from ppfft.legacy.ppfft import ppfft_horizontal, ppfft_vertical, ppfft, adj_ppfft
from ppfft.tools.grids import domain


def true_ppfft_horizontal(a):
    n, _ = a.shape
    m = 2 * n + 1

    ls = domain(n + 1)
    ks = domain(m)

    u_coords = -2 * ls[:, None] * ks[None, :] / n
    v_coords = np.tile(ks, (n + 1, 1))

    u = domain(n)
    v = domain(n)

    aux_u = np.einsum("u,lk->ulk", u, u_coords)
    aux_v = np.einsum("v,lk->vlk", v, v_coords)

    return np.einsum(
        "uv,ulk,vlk->lk",
        a,
        np.exp(-2j * np.pi * aux_u / m),
        np.exp(-2j * np.pi * aux_v / m),
    )


@pytest.mark.parametrize("n", [100, 101])
def test_ppfft_horizontal(n):
    im = np.random.rand(n, n)
    assert np.allclose(ppfft_horizontal(im), true_ppfft_horizontal(im))


def true_ppfft_vertical(a):
    n, _ = a.shape
    m = 2 * n + 1

    ls = domain(n + 1)
    ks = domain(m)

    v_coords = -2 * ls[:, None] * ks[None, :] / n
    u_coords = np.tile(ks, (n + 1, 1))

    u = domain(n)
    v = domain(n)

    aux_u = np.einsum("u,lk->ulk", u, u_coords)
    aux_v = np.einsum("v,lk->vlk", v, v_coords)

    return np.einsum(
        "uv,ulk,vlk->lk",
        a,
        np.exp(-2j * np.pi * aux_u / m),
        np.exp(-2j * np.pi * aux_v / m),
    )


@pytest.mark.parametrize("n", [100, 101])
def test_ppfft_vertical(n):
    im = np.random.rand(n, n)
    assert np.allclose(ppfft_vertical(im), true_ppfft_vertical(im))


@pytest.mark.parametrize("n", [100, 101])
def test_adj_ppfft(n):
    im1 = np.random.rand(n, n)
    im2 = np.random.rand(n + 1, 2 * n + 1)
    im3 = np.random.rand(n + 1, 2 * n + 1)

    h, v = ppfft(im1)

    prod1 = np.vdot(h, im2) + np.vdot(v, im3)
    prod2 = np.vdot(im1, adj_ppfft(im2, im3))
    assert np.isclose(prod1, prod2)
