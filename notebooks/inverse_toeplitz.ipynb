{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.linalg import solve_toeplitz, matmul_toeplitz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fast resampling of univariate trigonometric polynomials\n",
    "\n",
    "In this notebook we will implement the method described in https://arxiv.org/pdf/1507.06174.pdf to resample univariate trigonometric polynomials. Let's say we have a trigonometric polynomial:\n",
    "$$\n",
    "f : x \\mapsto \\sum_{k=-n/2}^{n/2-1} \\alpha_k \\exp\\left(i k x\\right)\n",
    "$$\n",
    "\n",
    "and we know, for a set of points $(y_1, \\dots, y_N) \\in [-\\pi, \\pi]^N$, its values $f(y_1), \\dots, f(y_N)$ with $N \\geq n$. We want to estimate, for a new set of points $(x_1, \\dots, x_M) \\in [-\\pi, \\pi]^M$, the values taken by $f$. This can be done by first finding $\\alpha$, by solving:\n",
    "$$\n",
    "\\min_{\\alpha \\in \\mathbb{C}^n} \\sum_{j=1}^N \\left|f(y_j) - \\sum_{k=-n/2}^{n/2-1} \\alpha_k \\exp\\left(i k y_j\\right)\\right|^2\n",
    "$$\n",
    "\n",
    "We use the following notations: $f = (f_1, \\dots, f_N)$ and $A_{j, k} = \\exp\\left( i k y_j\\right)$. We then have to solve:\n",
    "$$\n",
    "\\min_{\\alpha \\in \\mathbb{C}^n} \\|f - A \\alpha\\|^2\n",
    "$$\n",
    "\n",
    "The solution satisfies:\n",
    "$$\n",
    "A^\\dagger A \\alpha = A^\\dagger f\n",
    "$$\n",
    "\n",
    "We can precompute the inverse of $A^\\dagger A$ in the Gohberg-Semencul form, which states that we can apply it to $A^\\dagger f$ in $\\mathcal{O}(n \\log(n))$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "class InverseToeplitz:\n",
    "    \"\"\"A class for storing a Toeplitz matrix,\n",
    "    the Gohberg-Semencul decomposition of its inverse,\n",
    "    and use it to apply the inverse to a vector.\n",
    "\n",
    "    Methods\n",
    "    -------\n",
    "    apply_inverse\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, col: np.ndarray, row=None) -> None:\n",
    "        \"\"\"A Toeplitz matrix T is represented by:\n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        col : np.ndarray\n",
    "            First column of T.\n",
    "        row : np.ndarray | None\n",
    "            First row of T. By default None, meaning row = conj(col)\n",
    "\n",
    "\n",
    "        - its first column: col\n",
    "        - its first row: row\n",
    "\n",
    "        If row is not provided, it is assumed T is Hermitian.\n",
    "        \"\"\"\n",
    "        self.col = col\n",
    "        self.row = row\n",
    "\n",
    "        self.x0 = None\n",
    "        self.m1 = None\n",
    "        self.m2 = None\n",
    "        self.m3 = None\n",
    "        self.m4 = None\n",
    "\n",
    "        self.gohberg_semencul()\n",
    "\n",
    "    def gohberg_semencul(self) -> None:\n",
    "        \"\"\"Computes the Gohberg-Semencul decomposition of T^{-1}\"\"\"\n",
    "        e0 = np.zeros_like(self.col)\n",
    "        e0[0] = 1\n",
    "\n",
    "        e1 = np.zeros_like(self.col)\n",
    "        e1[-1] = 1\n",
    "\n",
    "        if self.row is None:\n",
    "            x = solve_toeplitz(self.col, e0)\n",
    "            y = solve_toeplitz(self.col, e1)\n",
    "\n",
    "        else:\n",
    "            x = solve_toeplitz((self.col, self.row), e0)\n",
    "            y = solve_toeplitz((self.col, self.row), e1)\n",
    "\n",
    "        x_a = np.zeros_like(x)\n",
    "        x_a[0] = x[0]\n",
    "\n",
    "        x_b = np.zeros_like(x)\n",
    "        x_b[1::] = x[:0:-1]\n",
    "\n",
    "        y_a = np.zeros_like(y)\n",
    "        y_a[0] = y[-1]\n",
    "\n",
    "        y_b = np.zeros_like(y)\n",
    "        y_b[1::] = y[:-1]\n",
    "\n",
    "        self.x0 = x[0]\n",
    "        self.m1 = (x, x_a)\n",
    "        self.m2 = (y_a, y[::-1])\n",
    "        self.m3 = (y_b, np.zeros_like(y))\n",
    "        self.m4 = (np.zeros_like(x), x_b)\n",
    "\n",
    "    def apply_inverse(self, vec: np.ndarray, workers: int = 8) -> np.ndarray:\n",
    "        \"\"\"Computes T^{-1} @ vec using the Gohberg-Semencul formula.\n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        vec : np.ndarray\n",
    "            Vector to compute the product T^{-1} @ vec.\n",
    "        workers : int\n",
    "            Workers parameter for scipy.linalg.matmul_toeplitz, by default 8\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        out : np.ndarray\n",
    "            Value of T^{-1} @ vec\n",
    "        \"\"\"\n",
    "        M1M2_v = matmul_toeplitz(\n",
    "            self.m1, matmul_toeplitz(self.m2, vec, workers), workers\n",
    "        )\n",
    "\n",
    "        M3M4_v = matmul_toeplitz(\n",
    "            self.m3, matmul_toeplitz(self.m4, vec, workers), workers\n",
    "        )\n",
    "\n",
    "        return (M1M2_v - M3M4_v) / self.x0"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fast version\n",
    "\n",
    "We first check how to do `matmul_toeplitz` manually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "def my_matmul_toeplitz(c, x, r=None):\n",
    "    n = len(x)\n",
    "    if r is None:\n",
    "        r = np.conjugate(c)\n",
    "\n",
    "    cr = np.concatenate((r[:0:-1], c)) # size 2n - 1\n",
    "        \n",
    "    return np.fft.ifft(np.fft.fft(x, n=3*n-2) * np.fft.fft(cr, n=3*n-2))[n-1:2*n-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "True\n",
      "True\n",
      "True\n",
      "True\n"
     ]
    }
   ],
   "source": [
    "n = 10\n",
    "c = np.random.rand(n)\n",
    "r = np.random.rand(n)\n",
    "x = np.random.rand(n)\n",
    "print(np.allclose(my_matmul_toeplitz(c, x, r), matmul_toeplitz((c, r), x)))\n",
    "print(np.allclose(my_matmul_toeplitz(c, x), matmul_toeplitz(c, x)))\n",
    "\n",
    "n = 11\n",
    "c = np.random.rand(n)\n",
    "r = np.random.rand(n)\n",
    "x = np.random.rand(n)\n",
    "print(np.allclose(my_matmul_toeplitz(c, x, r), matmul_toeplitz((c, r), x)))\n",
    "print(np.allclose(my_matmul_toeplitz(c, x), matmul_toeplitz(c, x)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "class FastInverseToeplitz:\n",
    "    \"\"\"A class for storing a Toeplitz matrix,\n",
    "    the Gohberg-Semencul decomposition of its inverse,\n",
    "    and use it to apply the inverse to a vector.\n",
    "\n",
    "    Methods\n",
    "    -------\n",
    "    apply_inverse\n",
    "    \"\"\"\n",
    "\n",
    "    def __init__(self, col: np.ndarray, row=None) -> None:\n",
    "        \"\"\"A Toeplitz matrix T is represented by:\n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        col : np.ndarray\n",
    "            First column of T.\n",
    "        row : np.ndarray | None\n",
    "            First row of T. By default None, meaning row = conj(col)\n",
    "\n",
    "\n",
    "        - its first column: col\n",
    "        - its first row: row\n",
    "\n",
    "        If row is not provided, it is assumed T is Hermitian.\n",
    "        \"\"\"\n",
    "        self.col = col\n",
    "        self.row = row\n",
    "        self.n = len(col)\n",
    "\n",
    "        self.x0 = None\n",
    "        self.fft_m1 = None\n",
    "        self.fft_m2 = None\n",
    "        self.fft_m3 = None\n",
    "        self.fft_m4 = None\n",
    "\n",
    "        self.gohberg_semencul()\n",
    "\n",
    "    def gohberg_semencul(self) -> None:\n",
    "        \"\"\"Computes the Gohberg-Semencul decomposition of T^{-1}\"\"\"\n",
    "        e0 = np.zeros_like(self.col)\n",
    "        e0[0] = 1\n",
    "\n",
    "        e1 = np.zeros_like(self.col)\n",
    "        e1[-1] = 1\n",
    "\n",
    "        if self.row is None:\n",
    "            x = solve_toeplitz(self.col, e0)\n",
    "            y = solve_toeplitz(self.col, e1)\n",
    "\n",
    "        else:\n",
    "            x = solve_toeplitz((self.col, self.row), e0)\n",
    "            y = solve_toeplitz((self.col, self.row), e1)\n",
    "\n",
    "        self.fft_m1 = np.fft.fft(np.concatenate((np.zeros(n-1), x)))\n",
    "\n",
    "        self.fft_m4 = np.fft.fft(np.concatenate((x[1:], np.zeros(n))))\n",
    "\n",
    "        col_m2 = np.zeros_like(y)\n",
    "        col_m2[0] = y[-1]\n",
    "        self.fft_m2 = np.fft.fft(np.concatenate((y[:-1], col_m2)))\n",
    "\n",
    "        col_m3 = np.zeros_like(y)\n",
    "        col_m3[1::] = y[:-1]\n",
    "        self.fft_m3 = np.fft.fft(np.concatenate((np.zeros(n-1), col_m3)))\n",
    "\n",
    "        self.x0 = x[0]\n",
    "\n",
    "    def apply_inverse(self, x: np.ndarray) -> np.ndarray:\n",
    "        \"\"\"Computes T^{-1} @ vec using the Gohberg-Semencul formula.\n",
    "        Uses np.fft 6 times instead of 12.\n",
    "\n",
    "        Parameters\n",
    "        ----------\n",
    "        x : np.ndarray\n",
    "            Vector to compute the product T^{-1} @ x.\n",
    "        workers : int\n",
    "            Workers parameter for scipy.linalg.matmul_toeplitz, by default 8\n",
    "\n",
    "        Returns\n",
    "        -------\n",
    "        out : np.ndarray\n",
    "            Value of T^{-1} @ x\n",
    "        \"\"\"\n",
    "\n",
    "        fft_x = np.fft.fft(x, n=2*self.n-1)\n",
    "\n",
    "        m2_x = np.fft.ifft(self.fft_m2 * fft_x)[-self.n:]\n",
    "        m4_x = np.fft.ifft(self.fft_m4 * fft_x)[-self.n:]\n",
    "\n",
    "        res = np.fft.ifft(self.fft_m1 * np.fft.fft(m2_x, n=2*self.n-1) - self.fft_m3 * np.fft.fft(m4_x, n=2*self.n-1))[-self.n:]\n",
    "\n",
    "        return res / self.x0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[-0.06070225-0.38219609j -0.0441928 +0.2133863j  -0.0695824 +0.13299064j\n",
      "  0.00784348+0.01905012j  0.02498044-0.02564702j -0.16847488+0.03740223j\n",
      "  0.21449808+0.14024091j -0.12582266-0.15322444j -0.06383554+0.05421789j\n",
      "  0.20973099+0.17611828j]\n",
      "[ 0.24270567+0.19390203j  0.02968738+0.1319017j   0.01694945-0.00899186j\n",
      "  0.19208448-0.08689078j -0.13590862+0.09832271j  0.26050271-0.07767263j\n",
      " -0.04436297-0.01133576j  0.05937902-0.08769083j  0.09408789-0.06865334j\n",
      " -0.06070225-0.38219609j]\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "n = 10\n",
    "c = np.random.rand(n) + 1j * np.random.rand(n)\n",
    "x = np.random.rand(n) + 1j * np.random.rand(n)\n",
    "\n",
    "old_top = InverseToeplitz(c)\n",
    "new_top = FastInverseToeplitz(c)\n",
    "\n",
    "np.allclose(old_top.apply_inverse(x), new_top.apply_inverse(x))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "metadata": {},
   "outputs": [],
   "source": [
    "n = 2048\n",
    "c = np.random.rand(n) + 1j * np.random.rand(n)\n",
    "x = np.random.rand(n) + 1j * np.random.rand(n)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1.31 ms ± 46.9 µs per loop (mean ± std. dev. of 7 runs, 1,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit old_top.apply_inverse(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "531 µs ± 2.3 µs per loop (mean ± std. dev. of 7 runs, 1,000 loops each)\n"
     ]
    }
   ],
   "source": [
    "%timeit new_top.apply_inverse(x)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "py3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
